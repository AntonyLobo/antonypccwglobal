package com.pccw.pages;

import com.pccw.base.TestBase;

public class GoogleHomePage extends TestBase{

	
	public GoogleImagesPage goToImages() {
		click("imagesLink_XPATH");
		return new GoogleImagesPage();
	}
	
	
	
}
