package com.pccw.pages;

import com.pccw.base.TestBase;

public class GoogleImagesPage extends TestBase {
	
	public GoogleResultPage uploadImage() {
		click("searchImageIcon_XPATH");
		click("uploadImageTab_XPATH");
        String filePath = System.getProperty("user.dir") +"\\src\\test\\resources\\images\\MRF.png";
		type("imagePath_XPATH",filePath);
		return new GoogleResultPage();
	}

}
