package com.pccw.pages;

import static org.testng.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.pccw.base.TestBase;

public class GoogleResultPage extends TestBase{

	public static List<WebElement> allLinks= driver.findElements(By.xpath(OR.getProperty("results_XPATH")));
	
		public void clickResultLink() {	
			allLinks.get(Integer.parseInt(config.getProperty("visit_result"))).click();
		}
		
		public void verifyResult(String resultValue) {
//		    allLinks = driver.findElements(By.xpath(OR.getProperty("results_XPATH")));
			for(WebElement link : allLinks) {
				assertTrue(link.getText().contains(resultValue));
			}
		}
		
		
}
