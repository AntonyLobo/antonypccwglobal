package com.pccw.testcases;

import org.testng.annotations.Test;

import com.pccw.base.TestBase;
import com.pccw.pages.GoogleHomePage;
import com.pccw.pages.GoogleImagesPage;
import com.pccw.pages.GoogleResultPage;


public class UploadAndVerifyImageIntoGoogleTest extends TestBase{

	@Test
	public void uploadAndVerify() {
		
		GoogleHomePage homepage = new GoogleHomePage();
		GoogleImagesPage imagePage =homepage.goToImages();
		GoogleResultPage resultPage= imagePage.uploadImage();
		resultPage.verifyResult("MRF");
		resultPage.clickResultLink();
		UploadAndVerifyImageIntoGoogleTest.captureScreenShot(driver);
	}
}
